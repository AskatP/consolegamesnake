﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleSnakeGame
{
    class Apple
    {
        //Символ яблока
        static public char apple = 'o';

        //Для рандомного появления яблока
        static Random random = new Random();

        //Координаты для яблока
        static int appleFieldWidth;
        static int appleFieldHeight;

        //Счет
        public static int score = -1;

        //Метка чтобы яблоко не появилось на теле змеи
        static bool freeZone = true;

        //Метод для рандомного появления яблока
        public static void AppleSpawn()
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.Green;

            appleFieldWidth = random.Next(0, Console.WindowWidth);
            appleFieldHeight = random.Next(0, Console.WindowHeight);

            Console.SetCursorPosition(appleFieldWidth, appleFieldHeight);
            CheckSnakePosition(appleFieldWidth,appleFieldHeight);
            if (freeZone == true)
            {
                Console.Write(apple);
                score++;
            }

            Console.ResetColor();
        }

        //Если позиция будушего яблока окажется на координатах змеи то Мето появления яблока будет вызван по новой!
        static void CheckSnakePosition(int y, int x )
        {
            char outChar = '*';
            char[] readBuffer = new char[1];
            int readCount;

            CheckSymbol.ReadConsoleOutputCharacter(CheckSymbol.GetStdHandle(-11), readBuffer, 1, new CheckSymbol.COORD() { X = (short)y, Y = (short)x }, out readCount);
            if (readBuffer[0] == outChar)
            {
                AppleSpawn();
                freeZone = false;
            }
            else
            {
                freeZone = true;
            }
        }
    }
}
